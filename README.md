# 🔭 Looking Glass

An AI algorithm that makes predictions of the future of a data stream.

See the future, today.

* **Hyper-Realtime Prediction**: Predict the future of a data stream 10-20 seconds in the future and act on the prediction now

* **Constant Learning**: Automatically update predictions based on the actual result to learn to be more accurate

* **Lag Resistance**: Slow network? No problem. With enough data, looking-glass will discover what to expect and when to expect it. If the stream is predictable, you can act on those predictions and increasingly reduce the latency of your network


## 📝 Installation

```
npm install looking-glass
```
## 📝 Example

```typescript
import LookingGlass from 'looking-glass'

const lookingGlass = new LookingGlass()

myApp.subscribeToDataStream(subscriptionHandler)

function subscriptionHandler (data: any) {
	// Feed your data to the looking-glass.
	lookingGlass.feed(data)
}

const timeInFuture = 10

// Get a prediction of the data 10 seconds in the future.
const myFutureData = lookingGlass.getPrediction(timeInFuture)
```

## 👓 Use cases
- Multiplayer video games: player action prediction

## Demo Gif

![Looking Glass](https://media.giphy.com/media/yv1ggi3Cbase05a8iS/giphy.gif)
